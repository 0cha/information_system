# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Notice.destroy_all
Schedule.destroy_all
Notice.create(body: 'お知らせ' * 25)
beginning_of_month = Date.today.beginning_of_month
end_month = beginning_of_month.end_of_month
(beginning_of_month..end_month).map do |date|
  params = {
    date: date,
    body: '予定' * 50,
    f1_process_1: "○検査",
    f1_process_2: "◆工程会議",
    f1_process_3: "▲大素地",
    f1_process_4: "■祈願祭",
    f1_process_5: "○その他",

    f1_process_color_1: "type01",
    f1_process_color_2: "type02",
    f1_process_color_3: "type03",
    f1_process_color_4: "type04",
    f1_process_color_5: "type05",
    
    f2_process_1: "検査: #{rand(6) + 1}",
    f2_process_2: "検査: #{rand(6) + 1}",
    f2_process_3: "検査: #{rand(6) + 1}",
    f2_process_4: "検査: #{rand(6) + 1}",
    f2_process_5: "検査: #{rand(6) + 1}",

    f2_process_color_1: "type01",
    f2_process_color_2: "type02",
    f2_process_color_3: "type03",
    f2_process_color_4: "type04",
    f2_process_color_5: "type05",
    
    note_1: "備考: #{rand(6) + 1}",
    note_2: "備考: #{rand(6) + 1}",
    note_3: "備考: #{rand(6) + 1}",
    note_4: "備考: #{rand(6) + 1}",
    note_5: "備考: #{rand(6) + 1}",

    note_color_1: "type01",
    note_color_2: "type02",
    note_color_3: "type03",
    note_color_4: "type04",
    note_color_5: "type05",

    visitor_title: 'ようこそ株式会社アイビーへ',
    visitor_body: "◉◉◉◉◉株式会社様\n" * 5,
  }
  Schedule.create(params)
end
