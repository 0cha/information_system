class CreateVisitors < ActiveRecord::Migration
  def change
    create_table :visitors do |t|
      t.references :schedule
      t.text :body
      t.string :color
    end
  end
end
