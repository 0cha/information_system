class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.date :date
      t.integer :date_type
      t.text :body
      t.text :visitor_template      
      t.string :visitor_title
      t.string :f1_process_1
      t.string :f1_process_2
      t.string :f1_process_3
      t.string :f1_process_4
      t.string :f1_process_5
      t.string :f1_process_color_1
      t.string :f1_process_color_2
      t.string :f1_process_color_3
      t.string :f1_process_color_4
      t.string :f1_process_color_5
      
      t.string :f2_process_1
      t.string :f2_process_2
      t.string :f2_process_3
      t.string :f2_process_4
      t.string :f2_process_5
      t.string :f2_process_color_1
      t.string :f2_process_color_2
      t.string :f2_process_color_3
      t.string :f2_process_color_4
      t.string :f2_process_color_5
      
      t.string :note_1
      t.string :note_2
      t.string :note_3
      t.string :note_4
      t.string :note_5
      t.string :note_color_1
      t.string :note_color_2
      t.string :note_color_3
      t.string :note_color_4
      t.string :note_color_5      
      
    end
  end
end
