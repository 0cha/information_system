class AddColumnVisitor < ActiveRecord::Migration
  def change
    add_column :schedules, :visitor_body, :text
  end
end
