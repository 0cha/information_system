# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160419060211) do

  create_table "information", force: :cascade do |t|
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notices", force: :cascade do |t|
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.date    "date"
    t.integer "date_type"
    t.text    "body"
    t.text    "visitor_template"
    t.string  "visitor_title"
    t.string  "f1_process_1"
    t.string  "f1_process_2"
    t.string  "f1_process_3"
    t.string  "f1_process_4"
    t.string  "f1_process_5"
    t.string  "f1_process_color_1"
    t.string  "f1_process_color_2"
    t.string  "f1_process_color_3"
    t.string  "f1_process_color_4"
    t.string  "f1_process_color_5"
    t.string  "f2_process_1"
    t.string  "f2_process_2"
    t.string  "f2_process_3"
    t.string  "f2_process_4"
    t.string  "f2_process_5"
    t.string  "f2_process_color_1"
    t.string  "f2_process_color_2"
    t.string  "f2_process_color_3"
    t.string  "f2_process_color_4"
    t.string  "f2_process_color_5"
    t.string  "note_1"
    t.string  "note_2"
    t.string  "note_3"
    t.string  "note_4"
    t.string  "note_5"
    t.string  "note_color_1"
    t.string  "note_color_2"
    t.string  "note_color_3"
    t.string  "note_color_4"
    t.string  "note_color_5"
    t.text    "visitor_body"
  end

  create_table "visitors", force: :cascade do |t|
    t.integer "schedule_id"
    t.text    "body"
    t.string  "color"
  end

end
