// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.cookie
//= require turbolinks
//= require jquery.turbolinks
//= require_tree .
$(document).ready(function(){
    var isMSIE = false;
    var userAgent = window.navigator.userAgent.toLowerCase();
    if( userAgent.match(/(msie|MSIE|edge)/) || userAgent.match(/(T|t)rident/) ) {
        isMSIE = true;
    } else {
        isMSIE = false;
    }    
    
    $("input[type=submit]").on('click', function(){
        var error = false;
        if ($("#notice_body").val().length > 100) {
            $("#notice_error").text('お知らせは100文字を超えないでください')
            error = true;
        }
        if ($("#schedule_body").val().length > 100) {
            $("#schedule_error").text('予定は100文字を超えないでください')
            error = true;
        }
        if ($("#schedule_visitor_body").val().length > 100) {
            $("#visitor_error").text('来客メッセージはは100文字を超えないでください')
            error = true;
        }
        
        if(error) {
            return false;
        }
    });
    $('#switch_inf').on('click', function(){
        var style = $.cookie('switch_inf');
        
        if(style == null || style == "normal"){
            if(isMSIE) {
                setCookie('switch_inf','expand', 367);
            } else {
                $.cookie('switch_inf','expand', {expire: 365, path: '/schedules' } );
            }
            var position = 0.0;
            
            position = $('.inf-container').first().parent().offset().top;
            $("html,body").animate({scrollTop: position},{queue: false});

        } else if(style == "expand"){
            if(isMSIE){
                setCookie('switch_inf','normal', 367);
            } else {
                $.cookie('switch_inf','normal', {expire: 365, path: '/schedules' } );
            }
        }
        switch_style();
    });
    switch_style = function() {
        var style = $.cookie('switch_inf');
        if(style == null || style == 'normal'){
            $('.plan-container').show();
            $('.inf-container').hide();
            $('.head_1').text('第一工場')
            $('.head_2').text('第二工場')            
        } else if(style == 'expand'){
            $('.plan-container').hide();
            $('.inf-container').show();
            $('.head_1').text('予定');
            $('.head_2').text('来客');            
        }
    }
    switch_style();
    // クッキー保存　setCookie(クッキー名, クッキーの値, クッキーの有効日数); //
    function setCookie(c_name,value,expiredays){
        // pathの指定
        var path = location.pathname;
        // pathをフォルダ毎に指定する場合のIE対策
        var paths = new Array();
        paths = path.split("/");
        if(paths[paths.length-1] != ""){
            paths[paths.length-1] = "";
            path = paths.join("/");
        }
        // 有効期限の日付
        var extime = new Date().getTime();
        var cltime = new Date(extime + (60*60*24*1000*expiredays));
        var exdate = cltime.toUTCString();
        // クッキーに保存する文字列を生成
        var s="";
        s += c_name +"="+ escape(value);// 値はエンコードしておく
        s += "; path="+ path;
        if(expiredays){
            s += "; expires=" +exdate+"; ";
        }else{
            s += "; ";
        }
        // クッキーに保存
        document.cookie=s;
    }    
});
