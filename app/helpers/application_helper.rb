module ApplicationHelper
  def schedule_calendar(week_length,link=false)
    content_tag :table, class: 'week' do
      head = content_tag :tr do
        concat content_tag(:th, '',class: 'top')
        concat content_tag(:td, '第一工場', class: 'top')
        concat content_tag(:td, '第二工場', class: 'top')
        concat content_tag(:td, '備考', class: 'top topRight')
      end
      concat head
      weeks_table = weeks(week_length).map do |date|
        schedule = @schedules.select{|schedule| schedule.date == date }.first

        if HolidayJp.holiday?(date)
          options = {class: 'holiday'}          
        elsif date.wday == 0
          options = {class: 'holiday'}
        end
        if schedule.present?
          if schedule.date_type == 1
            options = nil
          elsif schedule.date_type == 0
            options = {class: 'holiday'}
          end
        end
        

        content_tag :tr, options do
          concat content_tag(:th, I18n.l(date, format: :day), scope: 'row', class: "calendar_day_#{date.wday}")
          
          f1_process = content_tag(:td) do
            if schedule.present?
              (1..5).each do |i|
                concat content_tag(:span, eval("schedule.f1_process_#{i}"), class: eval("schedule.f1_process_color_#{i}"))
              end
            end
          end
          f2_process = content_tag(:td) do
            if schedule.present?
              (1..5).each do |i|
                concat content_tag(:span, eval("schedule.f2_process_#{i}"), class: eval("schedule.f2_process_color_#{i}"))
              end
            end
          end
          notes = content_tag(:td) do
            if schedule.present?
              (1..5).each do |i|
                concat content_tag(:span, eval("schedule.note_#{i}"), class: eval("schedule.note_color_#{i}"))
              end
            end
          end
          concat f1_process
          concat f2_process
          concat notes
        end
      end.join.html_safe
      concat weeks_table
    end
  end

  def weeks(size)
    current = @today
    last = current.weeks_since(size)
    return (current..last)
  end
  
  def get_date_type(date=false)
    if @schedule.blank?
      if date.wday == 0 || HolidayJp.holiday?(date)
        date_type = 0
      end
    elsif @schedule.date_type.blank?
      puts @schedule.date_type
      if @schedule.date.wday == 0 || HolidayJp.holiday?(@schedule.date)
        date_type = 0
      end
    else
      date_type = @schedule.date_type
    end
    
    return date_type
  end
  
end
