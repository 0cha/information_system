module SchedulesHelper
  def select_templates
    template_dir = File.join(Rails.root,'public','img','visitor','*.jpg')
    templates = []
    Dir.glob(template_dir) do |file|
      templates << File.basename(file)
    end
    return templates.sort
  end

  def get_plan(content,num)
    eval("@schedule.#{content}#{num}")
  end
  
  def get_plan_color(content,num)
    eval("@schedule.#{content}color_#{num}")
  end
  
end
