class Visitor < ActiveRecord::Base
  belongs_to :schedule
  scope :active, -> {where.not('body IS ? OR body = ?', nil, '')}
end
