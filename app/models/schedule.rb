class Schedule < ActiveRecord::Base
  has_many :visitors
  accepts_nested_attributes_for :visitors
  scope :between_date, -> (date_range){where(date: date_range)}
  DATE_TYPE = {
    0 => '休日',
    1 => '平日'
  }

  COLORS = {
    'type00' => '黒',
    'type01' => '赤',
    'type02' => '黄',
    'type03' => '緑',
    'type04' => '青',
    'type05' => 'ピンク'
  }

  CONTENTS = [
    "f1_process_",
    "f2_process_",
    "note_"
  ]
end
