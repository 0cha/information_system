class SchedulesController < ApplicationController
  def index
    @notice = Notice.first ? Notice.first : Notice.new
    if params[:date]
      @beginning_of_month = Date.parse(params[:date]).beginning_of_month
    else
      @beginning_of_month = Date.today.beginning_of_month
    end
    @end_of_month = @beginning_of_month.end_of_month
    date_range = (@beginning_of_month..@end_of_month)
    @schedules = Schedule.between_date(date_range)
  end
  
  def new
    @notice = Notice.first ? Notice.first : Notice.new
    @schedule = Schedule.find_or_initialize_by(date: params[:date])
    @visitor_title = @schedule.visitor_title.present? ? @schedule.visitor_title : 'ようこそ株式会社アイビーへ'
    if @schedule.date_type.blank?
      if @schedule.date.wday == 0 || HolidayJp.holiday?(@schedule.date)
        @schedule.date_type = 0
      else
        @schedule.date_type = 1
      end      
    end
  end

  def edit
  end

  def create
    @notice = Notice.find_or_initialize_by(id: 1)
    @notice.update(permit_notice_params)
    @schedule = Schedule.find_or_initialize_by(date: params[:schedule][:date])
    @schedule.update(permit_schedule_params)
    redirect_to action: :index, controller: :schedules, date: @schedule.date
  end

  private
  def permit_schedule_params
    params.require(:schedule).permit(
      :date,
      :body,
      :visitor_title,
      :visitor_body,
      :visitor_template,
      :date_type,
      :f1_process_1,
      :f1_process_2,
      :f1_process_3,
      :f1_process_4,
      :f1_process_5,
      :f2_process_1,
      :f2_process_2,
      :f2_process_3,
      :f2_process_4,
      :f2_process_5,
      :f1_process_color_1,
      :f1_process_color_2,
      :f1_process_color_3,
      :f1_process_color_4,
      :f1_process_color_5,
      :f2_process_color_1,
      :f2_process_color_2,
      :f2_process_color_3,
      :f2_process_color_4,
      :f2_process_color_5,
      :note_1,
      :note_2,
      :note_3,
      :note_4,
      :note_5,
      :note_color_1,
      :note_color_2,
      :note_color_3,
      :note_color_4,
      :note_color_5
    )
  end
  
  def permit_information_params
    params.require(:information).permit(:body)
  end

  def permit_visitor_params
    params.require(:visitor).permit(started_at: [], ended_at: [], body: [], template: [], color: [], id: [])
  end

  def permit_notice_params
    params.require(:notice).permit(:body)
  end

end
