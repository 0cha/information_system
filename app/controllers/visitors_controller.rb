class VisitorsController < ApplicationController
  def index
  end

  def new
  end

  def create
    Visitor.create(permit_visitor_params)
  end

  private
  def permit_visitor_params
    params.require(:visitor).permit(:body,:started_at, :ended_at, :template, :color)
  end
  
end
