class WelcomeController < ApplicationController
  layout 'information'
  def index
    if params[:date].present?
      @today = Date.parse(params[:date])
    else
      @today = Date.today
    end
    
    @schedule = Schedule.find_by(date: @today)
    render action: :index
  end
end
