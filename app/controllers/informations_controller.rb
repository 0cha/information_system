class InformationsController < ApplicationController
  layout 'information'
  
  def index
    @notice = Notice.first
    if params[:date].present?
      @today = Date.parse(params[:date])
    else
      @today = Date.today
    end

    two_weeks = @today.weeks_since(2)
    @schedule = Schedule.find_by(date: Date.today)
    @visitors = @schedule.visitors.where.not(body: [nil, '']) if @schedule.present?
    @schedules = Schedule.where(date: (@today..two_weeks))
    render action: :index
  end
  
end
